function clean(varargin)
% clean
% use clean --help or clean -h to get help

config = preset('clean', 'remove all output folders of a given preset', varargin);
if config.exit
    return;
end

folders_prefixes = {'compiled', 'dataset', 'clusters', 'clusters_vectors', 'results'};
folders = {'','','','',''};

for i=1:length(folders)
  folders{i} = sprintf('%s%s/%s',config.data_folder,folders_prefixes{i},config.(sprintf('%s_folder', folders_prefixes{i})));
end

if ~config.force
  folder_exist = 0;
  for i=1:length(folders)
    if exist(folders{i}, 'dir')
        folder_exist = 1;
        in = '';
        while(~strcmp(in,'y') && ~strcmp(in,'n'))
            fprintf('All output files of the preset ''%s'' will be deleted.\n', config.preset);
            in = input('Are you sure? [y/n] ', 's');
        end
        if ~strcmp(in,'y')
            error('Execution stopped by user.');
        else
            break;
        end
    end
  end
end

if(config.force || folder_exist)
  for i=1:length(folders)
    delete_dir_if_exist(folders{i});
  end
else
  warning('No output files were found for the preset''%s''', config.preset);
end