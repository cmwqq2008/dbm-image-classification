function runall(varargin)
% runall
% use runall --help or runall -h to get help

setup
config = preset('runall', 'run the pipeline for all the existent scripts', varargin);
if config.exit
    return;
end

resume = false;
if ~config.force
  folder = config.data_folder;
  if exist(folder, 'dir')
      in = '';
      while(~strcmp(in,'y') && ~strcmp(in,'n'))
          in = input('One or more output folders already exist.\nDo you want to RESUME last execution? [y/n] ', 's');
      end
      if strcmp(in,'y')
        resume = true;
      else
        in = '';
        while(~strcmp(in,'y') && ~strcmp(in,'n'))
            in = input('Do you want to OVERWRITE them and RESTART the execution? [y/n] ', 's');
        end
        if ~strcmp(in, 'y')
          error('Execution stopped by user.');
        end
      end
  else
      resume = true;
  end
end

fprintf('[preset:none][script:RUNALL] scanning existing presets...\n');
listing = dir('config');
cnt = 1;
presets = {};
for i = 1:length(listing)
    if(listing(i).isdir == 0)
        splitted = strsplit(listing(i).name,'_');
        if(~strcmp(splitted(1), 'default'))
          result_filename = sprintf('%sresults/%s/stats.mat', config.data_folder, splitted{1});
          if ~resume || (resume && ~exist(result_filename, 'file'))
            presets(cnt) = splitted(1);
            cnt = cnt+1;
          end
        end
    end
end

for i=1:length(presets)
  fprintf('[preset:none][script:RUNALL] launching pipeline for preset ''%s''\n', presets{i});
  if resume
    run_pipeline(presets{i});
  else
    run_pipeline(presets{i},'--force');
  end
end

fprintf('[preset:none][script:RUNALL] plotting results...\n');
dbplot;

fprintf('[preset:none][script:RUNALL] task terminated succesfully!\n');