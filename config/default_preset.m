% DEFAULT PRESET
%
% Questo preset funge da default, per fare un preset 'A' creare il
% file 'A_preset.m' e definire le variabili che devono essere
% diverse rispetto ai default. Tutte le altre variabili rimarranno
% invariate.

% La cartella delle immagini sorgente
config.src_folder = 'imageset/';

% La cartella in cui vengono salvati tutti i dati intermedi delle varie
% fasi dell'esecuzione
config.data_folder = 'data/';

% La sottocartella in cui vengono salvate le immagini compilate
config.compiled_folder = 'default/';

% La sottocartella in cui viene salvato il dataset
config.dataset_folder = 'default/';

% La sottocartella in cui vengono salvati i clusters
config.clusters_folder = 'default/';

% La sottocartella in cui vengono salvati i cluster vectors
config.clusters_vectors_folder = 'default/';

% La sottocartella in cui vengono salvati i risultati
config.results_folder = 'default/';

% I path da aggiungere
config.addpaths = {'lib/', 'lib/filesystem/', 'lib/sift-bin/', 'lib/progressbar/', 'lib/utils/'};

% La lunghezza di default delle progressbar (in caratteri)
config.progressbar_length = 50;

% L'altezza dell'immagine una volta compilata
config.img_height = NaN; % NaN sceglie l'altezza in base alla larghezza

% La larghezza dell'immagine una volta compilata
config.img_width = 80;

% L'operazione da svolgere come preprocessing in fase di compilazione
config.preprocess = 'gray';
% config.preprocess = 'thresh';

% Le immagini del training set
config.training_set = {...
    'building_01.jpg', 'building_02.jpg', 'building_03.jpg', 'building_04.jpg', 'building_05.jpg'...
    'building_06.jpg', 'building_07.jpg', 'building_08.jpg', 'building_09.jpg', 'building_10.jpg'...
    'car_01.jpg', 'car_02.jpg', 'car_03.jpg', 'car_04.jpg', 'car_05.jpg'...
    'car_06.jpg', 'car_07.jpg', 'car_08.jpg', 'car_09.jpg', 'car_10.jpg'...
    'flower_01.jpg', 'flower_02.jpg', 'flower_03.jpg', 'flower_04.jpg', 'flower_05.jpg'...
    'flower_06.jpg', 'flower_07.jpg', 'flower_08.jpg', 'flower_09.jpg', 'flower_10.jpg'
    };

% Una roba di ottimizzazione del cluster
% config.kmeans_onlinephase = 'on';
config.kmeans_onlinephase = 'off';

% Massimo numero di iterazioni di kmeans
config.kmeans_maxIter = 100;

% Numero di ripetizioni della costruzione dei cluster
config.kmeans_replicates = 1;

% Metodo con cui kmeans sceglie i centroidi iniziali per ogni cluster
% config.kmeans_start = 'sample';
% config.kmeans_start = 'cluster';
% config.kmeans_start = 'uniform';
config.kmeans_start = 'cluster';

% Numero di cluster
config.kmeans_cluster_n = 10;

% metodo per calcolare il numero di cluster finale
config.kmeans_cluster_m = 'number';
%config.kmeans_cluster_m = 'relative_kp'; % relativo al numero di keypoints
%dell'universo

% metodo con cui viene stimata la distanza tra i vettori
config.class_distance = 'euclidean';
% config.class_distance = 'cosine';

% vengono valutate le prime ktop distanze dell'immagine per capirne la
% categoria
config.ktop = 1;

% categorie delle immagini
config.image_categories = {'building', 'car', 'flower'};

% ignora le seguenti immagini (dal dataset in poi)
% NOTA: verificare che nella seguente lista non ci sia una immagine del
% training set perch? build_dataset non lo supporta (di fatto si fida della
% lista config.training_set per calcolare il numero di immagini nel TS)
config.ignore_images = {''};
%config.ignore_images = {'flower_13.jpg'};
