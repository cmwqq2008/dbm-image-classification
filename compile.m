function compile(varargin)
% COMPILE
% use compile --help or compile -h to get help

config = preset('compile', 'compile source images and save to disk', varargin);
if config.exit
    return;
end

% manually check input folder (because out of data folder)
exist_dir_or_fail(config.src_folder);

% check folders
out_folder = check_folder('compiled', 'output', config);

% cell 1xN containing image data source
fprintf(config.labels);
fprintf('Loading source images...\n');
[images_cell, image_names_cell] = image_cell_load(config.src_folder);

% resize images to 320 pixels as width
fprintf(config.labels);
fprintf('Resizing the images to a resolution of %dx%d...\n', config.img_width, config.img_height);
images_cell = image_cell_resize(images_cell, config.img_height, config.img_width);

% convert to grayscale
fprintf(config.labels);
fprintf('Converting the images to grayscale...\n');
images_cell = image_cell_convert(images_cell, config.preprocess);

% write image to disk
fprintf(config.labels);
fprintf('Saving images to disk at: %s\n', out_folder);
exist_dir_or_create(out_folder);
image_cell_write(images_cell, image_names_cell, out_folder);