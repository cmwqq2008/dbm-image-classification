function [Classification, Ktop, Distances] = classification(varargin)
% classification
% use classification --help or classification -h to get help

config = preset('classification', 'executes a classification over the training set', varargin);
if config.exit
    return;
end

% check folders
in_folder = check_folder('clusters_vectors', 'input', config);
out_folder = check_folder('results', 'output', config);
kp_folder = check_folder('clusters', 'input', config);

% Load sift data with cluster vectors from file
fprintf(config.labels);
fprintf('Loading sift data with cluster vectors from files...\n');
SiftData = load_sift_data(in_folder, config);

% Load clusters
fprintf(config.labels);
fprintf('Loading clusters data from files...\n');
load(sprintf('%sdata',kp_folder));
kp_img = cluster_universe(:,1);

% local variables
cktop = config.ktop;
img_n = length(SiftData);
n_img_classes = length(config.image_categories);
% matching statistics
stats = struct;

% # image per each class (using config.image_categories)
img_per_classes = zeros(1,length(config.image_categories));

%% costruzione di due vettori; uno per gli indici delle immagini del
% training set, e uno per gli indici immagini non categorizzate


% vector for training set images ids
training_set_imgs = [];
% vector for non categorized images ids
non_categorized_imgs = [];
for i = 1:img_n
    % immagine nel training set
    if (SiftData(i).training_set)
       training_set_imgs = [training_set_imgs, SiftData(i).id];
    else
        non_categorized_imgs = [non_categorized_imgs, SiftData(i).id];
        class_id = find_cell_value(config.image_categories, SiftData(i).category);
        img_per_classes(class_id) = img_per_classes(class_id) + 1;
    end
end

%assignin('base', 'tset_id', training_set_imgs);
%assignin('base', 'noncat_id', non_categorized_imgs);

%% Calcolo delle distanze
% per ogni immagine del training set
% calcolare la distanza di ogni altra immagine da esso
Distances = zeros(length(training_set_imgs), length(non_categorized_imgs));
% Distances ? una matrice TxC, dove T ? il vettore delle immagini del
% training set, mentre C ? il vettore delle immagini non etichettate;
% dunque sulle per **ogni colonna ? rappresentata la distanza di una certa
% immagine da ogni immagine del training set**

for i = 1:length(training_set_imgs)
    for j = 1:length(non_categorized_imgs)
    	% calcola distanza

        % id dell'immagine del training set di cui caricare il
        % cluster vector
        ts_id = training_set_imgs(i);
        % id dell'immagine non categorizzata di cui caric. il cl.vec.
        noncat_id = non_categorized_imgs(j);

        if (strcmp(config.class_distance, 'euclidean'))
          Distances(i,j) = pow2(pdist([SiftData(noncat_id).cluster_vector; SiftData(ts_id).cluster_vector], config.class_distance));
        else % cosine, cityblock
          Distances(i,j) = pdist([SiftData(noncat_id).cluster_vector; SiftData(ts_id).cluster_vector], config.class_distance);
        end

    end
end

assignin('base', 'Distances', Distances);

% raccogli le ktop immagini pi? simili a quella in esame tra quelle del
% traning set (Distance > 0)
% in seguito prendi le categorie di queste immagini e prendi quella pi?
% rappresentativa

KtopDistances = zeros(length(non_categorized_imgs), cktop);
Classification = [];

for i = 1:length(non_categorized_imgs)

    class_roulette = zeros(1,n_img_classes);

    % distanze ordinate dell'immagine rispetto al training set
    [sorted_distances, imts_dist_idx] = sort(Distances(:,i));

    % prendiamo gli indici delle prime immagini del training set pi? vicine
    % all'immagine i
    KtopDistances(i, :) = imts_dist_idx(1:cktop);

    % per ogni immagine del training set incremento il valore di
    % un array (elemento i == classe dell'img)
    % di tre elementi che mappano le tre classi che abbiamo
    for j = 1:cktop
        % trovo l'indice della categoria dell'immagine del training set in esame
        img_ts_id = training_set_imgs(imts_dist_idx(j));
        cls_idx = find_cell_value(config.image_categories, SiftData(img_ts_id).category);
        % incremento di uno il counter
        class_roulette(cls_idx) = class_roulette(cls_idx) + 1 - sorted_distances(j)/max(sorted_distances);
    end


    [~, cat_idx] = max(class_roulette);
    s = struct(...
        'id', SiftData(non_categorized_imgs(i)).id,...
        'name', SiftData(non_categorized_imgs(i)).name,...
        'org_cat', SiftData(non_categorized_imgs(i)).category,...
        'rec_cat', config.image_categories(cat_idx));
    %sprintf('name: %s orig: %s recog:%s', SiftData(non_categorized_imgs(i)).name), SiftData(non_categorized_imgs(i)).category, config.image_categories(cat_idx));
    Classification{i} = s;
end

%assignin('base', 'Classification', Classification);

%% Statistics
img_cat = zeros(1,length(config.image_categories));
for i=1:length(Classification)
    if (strcmp(Classification{i}.org_cat, Classification{i}.rec_cat))
        cat_id = find_cell_value(config.image_categories, SiftData(Classification{i}.id).category);

        img_cat(cat_id) = img_cat(cat_id) + 1;
    end
end

assignin('base', 'img_cat', img_cat);
assignin('base', 'img_per_classes', img_per_classes);

stats.classes = {};
for i=1:length(config.image_categories)
    stats.classes{i} = {...
        char(config.image_categories(i)),...
        img_cat(i) / img_per_classes(i)
    };
end

% totale
stats.classes{i+1} = {...
    'total',...
    sum(img_cat) / sum(img_per_classes)
};

kp_img = size(universe,1);

% Numero di cluster
stats.n_clusters = calc_cluster_qty(config.kmeans_cluster_n, config.kmeans_cluster_m, kp_img);
% Numero relativo di cluster (in millesimi)
stats.rel_clusters = config.kmeans_cluster_n;
% Numero di immagini
stats.n_images = length(SiftData);
% Algoritmo usato per il clustering
stats.cluster_alg = 'k-means';
% Dunzione distanza utilizzata per la classificazione
stats.class_distance = config.class_distance;
% Altezza delle immagini
stats.img_height = config.img_height;
% Larghezza delle immagini
stats.img_width = config.img_width;
% Preprocessing eseguito sulle immagini in fase di compilazione
stats.preprocess = config.preprocess;
% Numero di immagini del training set
stats.n_training_set = length(config.training_set);
% Metodo utilizzato per definire i primi cluster
stats.kmeans_start = config.kmeans_start;


assignin('base', 'stats', stats);

fprintf(config.labels);
fprintf('Saving to file...\n');
exist_dir_or_create(out_folder);
for i=1:length(Classification)
    s = Classification{i};
    save(sprintf('%s%s.mat',out_folder,SiftData(non_categorized_imgs(i)).name), '-struct', 's');
end
save(sprintf('%sresults',out_folder), 'KtopDistances', 'Distances');
save(sprintf('%sstats',out_folder), '-struct', 'stats');
