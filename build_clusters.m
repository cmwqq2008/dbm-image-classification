function build_clusters(varargin)
% BUILD_CLUSTERS
% use build_clusters --help or build_clusters -h to get help

config = preset('build_clusters', 'build clusters from all the keypoints', varargin);
if config.exit
    return;
end

% check folders
in_folder = check_folder('dataset', 'input', config);
out_folder = check_folder('clusters', 'output', config);

% Load sift data from file
fprintf(config.labels);
fprintf('Loading sift data from files...\n');
SiftData = load_sift_data(in_folder, config);

% - clusterize
images_number = length(SiftData);

% keypoints universe U
universe = [];
% keypoints match matrix with image
keypoints_images = [];

% Load sift data from file
fprintf(config.labels);
fprintf('Creating the keypoints universe...\n');
for image_index = 1:images_number
    % image descriptors matrix, transposed
    desc = SiftData(image_index).descriptors';

    % number of keypoints in this image
    keypoints_number = size(desc,1);

    % creating Universe
    % concatenate this image keypoints, along the rows
    universe = [universe; desc];

    % creating the keypoints - image matching
    ki_matching = zeros(keypoints_number, 1);
    ki_matching(:) = SiftData(image_index).id;
    keypoints_images = [keypoints_images; ki_matching];
end

opts = statset(...
    'Display','iter'...
);

uni_n = size(universe,1);

%% cluster number calculation
cl_n = calc_cluster_qty(config.kmeans_cluster_n, config.kmeans_cluster_m, uni_n);
fprintf(config.labels);
fprintf('universe size %d...\n', uni_n);

if (strcmp(config.kmeans_cluster_m, 'relative_kp'))
  fprintf(config.labels);
  fprintf('percentuale cluster: %d...\n', config.kmeans_cluster_n);
end

fprintf(config.labels);
fprintf('clusterizing in %d...\n', cl_n);

%% Kmeans
fprintf(config.labels);
fprintf('Executing k-means...\n');


kmeans_distance = config.class_distance;
if (strcmp(kmeans_distance, 'euclidean'))
    kmeans_distance = 'sqEuclidean';
end
%assignin('base', 'kp_clu1', keypoints_cluster);
%assignin('base', 'kp_img', keypoints_images);
if strcmp(kmeans_distance,'sqEuclidean')
    [centroids, keypoints_cluster] = vl_kmeans(universe', cl_n, 'verbose', 'verbose', 'verbose');
    cluster_universe = [keypoints_images, keypoints_cluster'];
elseif strcmp(kmeans_distance,'cityblock')
    [centroids, keypoints_cluster] = vl_kmeans(universe', cl_n, 'verbose', 'verbose', 'verbose', 'distance', 'l1');
    cluster_universe = [keypoints_images, keypoints_cluster'];
elseif strcmp(kmeans_distance,'cosine')
    [keypoints_cluster, centroids, distance] = kmeans(universe, cl_n, ...
       'distance', kmeans_distance,...
       'onlinephase', config.kmeans_onlinephase,...
       'replicates', config.kmeans_replicates,...
       'start', config.kmeans_start,...
       'emptyaction', 'singleton',...
       'Options', opts);
   cluster_universe = [keypoints_images, keypoints_cluster];
end

%assignin('base', 'kp_clu2', keypoints_cluster2);



% Save!
fprintf(config.labels);
fprintf('Saving to file...\n');
exist_dir_or_create(out_folder);
save(sprintf('%sdata',out_folder), 'universe', 'cluster_universe');
