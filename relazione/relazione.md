<style>
body {
  font: normal medium/1.4 sans-serif;
}
table {
  border-collapse: collapse;
  width: 100%;
}
th, td {
  padding: 0.25rem;
  text-align: left;
  border: 1px solid #ccc;
}
tbody tr:nth-child(odd) {
  background: #eee;
}
</style>

# Classificatore di immagini automatico basato su un vocabolario di features salienti estratto dalle immagini.

Si &egrave; costruito un classificatore automatico di immagini utilizzando SIFT per l'estrazione delle features, K-means come algoritmo di clustering e k-top per la classificazione. Il progetto &egrave; stato sviluppato in Matlab, utilizzando anche la libreria VLFeat che fornisce un'implementazione di SIFT e K-means a prestazioni elevate.

Implementato da Ruben Caliandro e Roberto Pesando.

***Table of Contents***

[TOC]

## Usage

Scaricare la libreria [VLFeat](/http://www.vlfeat.org/download/vlfeat-0.9.18-bin.tar.gz), estrarla e inserirla in `lib/vlfeat`

Eseguire lo script ruby *create_presets.rb*

    ruby create_presets.rb

Aprire MATLAB ed eseguire

    runall

Attendere il termine dell'esecuzione (pu&ograve; volerci pi&ugrave; di un'ora). Verranno mostrati automaticamente i grafici dei risultati.

Dopo che &egrave; stata fatta partire l'esecuzione la prima volta, per vedere di nuovo i grafici, eseguire

    dbplot

## Caratteristiche del sistema

L'esercitazione &egrave; stata sviluppata a *fasi*, in modo da permettere uno sviluppo modulare e un comodo debug. Le varie fasi dell'esercitazione sono state sviluppate separatamente, in una struttura a pipeline. Per eseguire l'intera pipeline basta utilizzare lo script `runall.m` nella root del progetto.

Gli script, oltre a scrivere su stdout, scrivono dei files di output (nella cartella `data`) che servono sia come input per altri script che come file da consultare separatamente.

Il sistema, inoltre, &egrave; totalmente configurabile, essendo basato su *presets*: file di configurazione (nella cartella `config`) che impostano le variabili usate dalle cinque fasi della "pipeline" di classificazione. Nel *preset* `default` sono salvate le configurazioni predefinite, le quali vengono ereditate dai singoli *preset* che ne possono fare un override.

Il motivo dell'introduzione dei file di configurazione scaturisce dalla necessit&agrave; di automatizzare l'esecuzione degli algoritmi, per poter ripetere iterativamente l'intera pipeline con diverse impostazioni senza doverla configurare a mano. Adottare questo metodo ci ha permesso di eseguire un numero rilevante di test e di raccogliere centinaia di risultati da poter confrontare.

I *preset* permettono di configurare molte variabili. Le pi&ugrave; importanti sono:

* dimensione e colori delle immagini (gray o b/w)
* funzione distanza usata per kmeans e i cluster vectors
* numero di cluster creati (relativi o assoluti)
* k nell'algoritmo ktop
* training set (enumerazione delle immagini che ne fanno parte)
* categorie delle immagini e cartelle di lavoro

Al fine di combinare valori diversi per alcune variabili, ci &egrave; sembrato opportuno creare una serie di *preset* (esattamente 720), che derivano dal *prodotto cartesiano* delle possibili configurazioni:

* dimensione delle immagini: `[120, 240, 480]` pixels di larghezza (l'altezza viene scalata di conseguenza)
* metrica: `[euclidean, cosine, cityblock]`
* numero di cluster = `[0.1, 0.2, 0.4, 0.8, 1.6, 3.2, 6.4, 10]` percentuale di cluster rispetto al numero di keypoints.
* variabile k in ktop = `[1, 2, 3, 4, 5]`
* score function = `[constant, weighted_distance_cv]`

Questi valori possono essere impostati nello script ruby `create_preset.rb`, che permette di creare automaticamente tutti i file *preset*.

## Le fasi della classificazione

Le fasi della classificazione sono:

* __Fase 1__ : compilazione delle immagini, con dimensione variabile e colori in scala di grigio
* __Fase 2__ : creazione del dataset, usando SIFT
* __Fase 3__ : creazione dei cluster, usando Kmeans e metrica di distanza variabile
* __Fase 4__ : creazione del cluster vector per ogni immagine, usando $tf \times idf$
* __Fase 5__ : classificazione usando k-top, con distanza tra i cluster vector identica a quella usata per la clusterizzazione, k variabile e score calcolato o usando la distanza tra i cluster vectors o senza (ogni _voto_ vale uno).

### Fase I: Compile

Il nostro dataset consiste di 60 immagini: 3 classi (car, flower e building) e 20 immagini per ogni classe. 30 delle 60 immagini appartengono al training set (10 per ogni classe, quindi la met&agrave;).

La prima fase effettua il resize di tutte le immagini alla dimensione specificata dal *preset* e le trasforma in scala di grigio o threshold.

__Nota__: nei nostri test il threshold non ha fornito risultati soddisfacenti per cui abbiamo limitato l'azione alle sole immagini in scala di grigio.

Le immagini compilate vengono salvate nella cartella `data/compiled`

### Fase II: Build Dataset

Finita la __Fase I__, la fase della costruzione del dataset di immagini consiste nel caricamento delle immagini _compilate_ e nell'estrazione dei vettori di feature usando l'algoritmo `vl_sift` della libreria VLFeat (parametri predefiniti).

Per ogni immagine $i$ viene salvato il vettore di feature $v$, una matrice $ n \times 128$, dove $n$ &egrave; il numero di keypoints estratti dall'immagine $i$ e 128 il numero delle dimensioni di ogni keypoint.

In aggiunta al vettore di feature, vengono salvate informazioni quali nome del file e categoria, categoria riconosciuta, cluster vector.

Questi dati vengono scritti nella cartella `data/dataset`

### Fase III: Build Clusters

Usando due differenti parametri, numero di cluster e funzione distanza, viene usato l'algoritmo k-means per costruire il *vocabolario* di cluster che verr&agrave; utilizzato per la classificazione. Esso consiste in una matrice che contiene il match keypoint-clusterid.

Il *vocabolario* viene salvato nella cartella `data/clusters`

### Fase IV: Build Cluster Vectors

A partire dai cluster costruiamo per ogni immagine $i$, un cluster vector $cV_i : < w_{i1}, ..., w_{ij}, ..., w_{iN} > $, dove viene pesata l'appartenenza $w_{ij}$ del $j$-esimo cluster nell'immagine $i$ e $N$ &egrave; il numero di cluster esistenti. Il peso viene calcolato con la formula $tf \times idf$, dove $tf$ &egrave; la _cluster frequency_, $\frac{k_{ij}}{kp_{i}}$, dove $k_{ij}$ &egrave; il numero di keypoints dell'immagine $i$ mappati nel cluster $j$ e $kp_i$ &egrave; il numero di keypoints nell'immagine $i$. $idf$ invece &egrave; calcolato con la formula $log(\frac{I}{img_j})$, dove $I$ &egrave; la cardinalit&agrave; del database delle immagini e $img_j$ &egrave; il numero di immagini mappate nel cluster $j$.

I risultati vengono scritti nella cartella `data/clusters_vectors`

### Fase V: Classification con Top-k

La somiglianza tra le immagini del training set e le immagini da categorizzare (queries) viene effettuata tramite un calcolo della distanza tra i loro cluster vectors. Viene cos&igrave; costruita una matrice $t \times q$ di $t$ righe e $q$ colonne dove $t$ &egrave; il numero di immagini del training set e $q$ il numero di immagini da categorizzare.

Ogni colonna viene ordinata per distanza e vengono prese le prime k distanze di cui &egrave; valutata la categoria (sono immagini del training set!), creando cos&igrave; uno score. La funzione di score viene decisa dal *preset* corrente, e pu&ograve; essere:

* constant: ogni matching pesa $1$
* weighted_distance_cv: ogni matching pesa $1 - \frac{sorteddistances_{ij}}{max(sorteddistances_j)}$

dove:

* $sorteddistance_j$ &egrave; il vettore delle distanze tra tutte le immagini nel training set e l'immagine _query_ $j$;
* $sorteddistance_{ij}$ &egrave; la distanza tra l'immagine del training set $i$ &egrave; l'immagine _query_ $j$;
* $max(sorteddistances_j)$ &egrave; la distanza tra l'immagine $j$ e l'immagine pi&ugrave; del training set pi&ugrave; lontana;

Ogni match somma il proprio score parziale nella variabile di score della classe a cui appartiene. Alla fine dell'esecuzione, viene selezionata la classe che ha un punteggio pi&ugrave; alto.

Infine, vengono raccolte delle statistiche e salvate nella cartella `data/results` insieme ai risultati di ogni singola immagine.

## Risultati

### Guida alla lettura dei risultati

Come accennato precedentemente, abbiamo utilizzato 720 preset per automatizzare l'esecuzione della pipeline con diverse configurazioni. L'idea era quella di avere un riscontro visualizzabile su un gran numero di risultati.

I 720 preset sono il risultato di un *prodotto cartesiano* tra tutte le variabili che abbiamo deciso di testare. Questo vuol dire che l'intera esercitazione &egrave; stata eseguita 720 volte, una per ogni combinazione di:

* **width** - larghezza delle immagini in pixel
    - 120
    - 240
    - 480
* **distance_function** - metrica utilizzata per kmeans e classificazione
    - euclidean
    - cosine
    - cityblock
* **kmeans_n** - percentuale di cluster rispetto al numero totale di keypoints
    - 0.1
    - 0.2
    - 0.4
    - 0.8
    - 1.6
    - 3.2
    - 6.4
    - 10
* **k** - la variabile k di ktop
    - 1
    - 2
    - 3
    - 4
    - 5
* **score_function**
    - constant (tutti i matching valgono 1)
    - weighted_distance_cv (il valore dei matching &egrave; una funzione della loro distanza dalla query)

Per ognuno dei 720 *preset*, il risultato &egrave; una percentuale di **matching**, che &egrave; in funzione delle 5 variabili descritte sopra.

Per mostrare correttamente i valori di **matching** ottenuti, avremmo bisogno di uno spazio a 6 dimensioni (5 per le variabili e 1 per il **matching**). Dato che questo non &egrave; possibile, abbiamo deciso di privilegiare le variabili **kmeans_n** e **k**. 

In questo modo &egrave; possibile visualizzare una mesh che mostra i valori di **matching** in funzione **kmeans_n** e **k**, a parit&agrave; di **width**, **distance_function** e **score_function**

Esempio: grafico dei valori di matching con: **width** = 480; **distance** = cosine; **score_function** = constant

[![Img](example.png)](example.png)

Per visualizzare anche la variazione delle variabili **width** e **distance_function** abbiamo deciso di mostrare diverse mesh disposte in una griglia all'interno della stessa finestra. Le righe della griglia mostrano diversi valori di **width**, mentre **distance_function** varia a seconda delle colonne.

Per **score_function** abbiamo invece dovuto utilizzare due diverse finestre. Mostriamo il seguente schema per far capire meglio come abbiamo deciso di mostrare le 6 dimensioni descritte finora.

[![Img](schema.png)](schema.png)

### Risultati

Abbiamo raccolto sia i risultati parziali (% matching per una data classe) che i risultati globali (% matching per tutte le classi)

#### Risultati Parziali (classe building)

Grafici dei risultati per **score_function** = constant

[![Img](kt_building.png)](kt_building.png)

Grafici dei risultati per **score_function** = weighted_distance_cv

[![Img](ktwd_building.png)](ktwd_building.png)

#### Risultati Parziali (classe flower)

Grafici dei risultati per **score_function** = constant

[![Img](kt_flower.png)](kt_flower.png)

Grafici dei risultati per **score_function** = weighted_distance_cv

[![Img](ktwd_flower.png)](ktwd_flower.png)

#### Risultati Parziali (classe car)

Grafici dei risultati per **score_function** = constant

[![Img](kt_car.png)](kt_car.png)

Grafici dei risultati per **score_function** = weighted_distance_cv

[![Img](ktwd_car.png)](ktwd_car.png)

#### Risultati Globali (tutte le classi)

Grafici dei risultati per **score_function** = constant

[![Img](kt_total.png)](kt_total.png)

Grafici dei risultati per **score_function** = weighted_distance_cv

[![Img](ktwd_total.png)](ktwd_total.png)

### Commento sui risultati

#### Distanze

Osservando gi&agrave; i risultati globali, si nota che la **distanza euclidea** non &egrave; molto precisa, soprattutto per le immagini con **width = 480**. 

Dai risultati parziali, si capisce che la causa di questa imprecisione &egrave; dovuta alla classificazione errata di fiori e automobili, che non vengono riconosciuti correttamente. Gli edifici, invece, vengono riconosciuti con una **percentuale di matching** che in molti casi tocca il 100%. 

Il motivo per cui gli edifici vengono riconosciuti con una percentuale cos&igrave; alta &egrave; il seguente: la maggiorparte delle features salienti sono gli spigoli e i vertici delle finestre e dei muri. Questi spigoli e vertici hanno due propriet&agrave; importanti:

1. Hanno una forma regolare: gli spigoli sono segmenti di linea retta e i vertici formano angoli di 90 gradi. Queste forme vanno a finire sempre nelle stesse posizioni della maschera utilizzata da SIFT, quindi risultano in features con direzione simile.
2. Gli spigoli/vertici sono netti, cio&egrave; la transizione dell'intensit&agrave; luminosa in quei punti cresce/decresce rapidamente e questo si traduce in valori molto alti di gradiente (in valore assoluto). Questo fa s&igrave; che le features che ne vengono estratte avranno con buona probabilit&agrave; un modulo simile. Questo &egrave; un punto a favore per la **distanza euclidea**, dato che &egrave; sensibile a variazioni di modulo.

Queste due propriet&agrave; fanno si che i keypoints degli edifici cadano con una buona probabilit&agrave; sempre negli stessi cluster.

I fiori e le automobili, invece, presentano features che possono anche avere moduli molto diversi tra di loro quindi, mancando la regolarit&agrave; che si osservava negli edifici, i keypoint vanno a *disperdersi* su diversi cluster che potrebbero anche avere direzione simile, ma hanno modulo diverso. A prova di ci&ograve;, si osserva infatti che la **percentuale di matching** (per fiori ed edifici) ***decresce all'aumentare del numero di cluster***, perch&egrave; con pi&ugrave; cluster aumenta la *dispersione* dei keypoint.

Per quanto riguarda invece le immagini piccole (**width = 120**), la scarsa definizione fa perdere precisione alla **distanza euclidea** sugli edifici (spigoli e vertici sono meno definiti), ma ne acquista sui fiori, che sono le immagini pi&ugrave; cariche di dettagli (quali striature di colore molto fini). Nel caso dei fiori, infatti, la bassa definizione livella le differenze di trama tra un fiore e l'altro, riducendo il rumore sui keypoint estratti.

La **distanza dei coseni** &egrave; la pi&ugrave; precisa a livello globale, cos&igrave; come sulle singole classi, mentre la **distanza di manhattan** (cityblock) &egrave; di difficile interpretazione, perch&egrave; i risultati globali sono apparentemente buoni e stabili ma i risultati parziali su ogni singola classe sembrano parecchio altalenanti al variare del **numero di cluster**.

#### Percentuale di cluster

Per quanto riguarda la **percentuale di cluster**, osservando i risultati globali si nota che i grafici seguono un andamento simile: precisione del 33% (quindi nulla, dato che le classi sono 3) con 0.1% di cluster, che cresce insieme al numero di cluster. Si nota per&ograve; che oltre una certa percentuale di cluster (mediamente 3.2%) il matching non migliora: a volte rimane stabile (o migliora di poco), a volte addirittura decresce, come nel caso della distanza euclidea (questo fenomeno &egrave; stato spiegato prima).

#### Ktop

La maggior parte dei grafici ha un andamento costante sull'asse **ktop_n**, questo significa che far variare **k** (cio&egrave; classificare le immagini in base ai primi **k** risultati) non &egrave; cos&igrave; rilevante. Questo pu&ograve; essere visto come un fattore positivo per i seguenti motivi:

1. Si ha una precisione alta (in alcuni casi > 80%) gi&agrave; con k=1, quindi l'approccio utilizzato funziona bene. (sarebbe difficile migliorarlo con cos&igrave; poche immagini)
2. La precisione rimane costante per k>1 e non peggiora usando una funzione di score costante. Si capisce meglio con un esempio: supponiamo k=5, ci sono 10 immagini corrette e 20 sbagliate. Alla prima iterazione, se l'immagine restituita &egrave; corretta ne rimangono solo pi&ugrave; 9 della classe giusta. Se alle iterazioni successive ne vengono trovate altre corrette, ho sempre pi&ugrave; probabilit&agrave; di scegliere immagini sbagliate. Questo ovviamente non sarebbe un problema se si avessero pi&ugrave; immagini, perch&egrave; il numero di immagini del training set sarebbe molto maggiore rispetto a k. Nel nostro caso, invece, k assume valori molto alti in rapporto alle immagini corrette (per esempio k=5 e immagini corrette=10), e questo &egrave; statisticamente problematico.  

Il problema spiegato al punto 2. sopra &egrave; stato parzialmente risolto con il cambiamento della funzione di score. Il passaggio da uno score costante ad una funzione che pesa la distanza (vd. [Fase V: Classification con Top-k](#fase-v-classification-con-top-k)) ha permesso di minimizzare il peso delle immagini del training set _meno somiglianti_ aumentando cos&igrave; la percentuale di matching globale con picchi del 20%.

    480px|ktop5|cluster3.2%

|                   | global (cosine) | global (cityblock) | global (euclidean) |
|-------------------|-----------------|--------------------|--------------------|
| constant score    | 76%             | 66%                | 70%                |
| w. distance score | 83%             | 66%                | 76%                |

    240px|ktop3|cluster3.2%

|                   | global (cosine) | global (cityblock) | global (euclidean) |
|-------------------|-----------------|--------------------|--------------------|
| constant score    | 76,7%           | 43,3%              | 70%                |
| w. distance score | 93%             | 63,3%              | 70%                |

I risultati migliori, sono stati trovati con i seguenti parametri:

* **width**: 240
* **distance_function**: cosine
* **score_function**: weighted_distance_cv

| cluster\k |  1  |  2  |  3  |  4  |  5  |
| :-------: | --- | --- | --- | --- | --- |
|  **10%**  | 80% | 80% | 83% | 80% | 80% |
|  **6.4%** | 70% | 70% | 70% | 76% | 83% |
|  **3.2%** | 76% | 76% | 93% | 76% | 76% |
|  **1.6%** | 73% | 73% | 73% | 73% | 76% |
|  **0.8%** | 73% | 73% | 80% | 80% | 80% |
|  **0.4%** | 66% | 66% | 70% | 70% | 63% |
|  **0.2%** | 70% | 70% | 73% | 73% | 76% |
|  **0.1%** | 33% | 33% | 33% | 33% | 33% |

Come si vede dalla tabella, i migliori in assoluto sono quelli con percentuale di cluster 3.2% e k = 3.
