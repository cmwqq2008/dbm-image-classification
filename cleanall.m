function cleanall(varargin)
% cleanall
% use cleanall --help or cleanall -h to get help

config = preset('cleanall', 'remove all output folders of all the presets', varargin);
if config.exit
    return;
end


if ~config.force
  folder_exist = 0;
  if exist(config.data_folder, 'dir')
      folder_exist = 1;
      in = '';
      while(~strcmp(in,'y') && ~strcmp(in,'n'))
          fprintf('All output files of all the presets will be deleted.\n');
          in = input('Are you sure? [y/n] ', 's');
      end
      if ~strcmp(in,'y')
          error('Execution stopped by user.');
      end
  end
end

if(config.force || folder_exist)
  delete_dir_if_exist(config.data_folder);
else
  warning('No output files were found');
end