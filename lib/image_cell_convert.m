function ImageData = image_cell_convert(ImageDataSrc, type)
% convert to grayscale (0 false, 1 true)
% threshold (0 false, 1 true)

% save the rows of image cell in order to know how many images we have
images_n = size(ImageDataSrc,2);

progbar = progressbar_init(50, images_n);

for img_idx = 1:images_n
    % image matrix
    I = cell2mat(ImageDataSrc(img_idx));
    
    % convert image to grayscale
    if (strcmp(type, 'gray'))
        ImageData{img_idx} = rgb2gray(I);
    end
    
    % threshold an image (we could use OTSU)
    % white remains white, other colors become black
    if (strcmp(type, 'thresh'))
        ImageData{img_idx} = im2bw(I,graythresh(I));
    end
    
    progbar = progressbar_step(progbar);
end

