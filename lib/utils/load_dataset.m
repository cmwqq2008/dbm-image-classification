function SiftData = load_dataset(varargin)
% LOAD_DATASET
% use load_dataset --help or load_dataset -h to get help

config = preset('load_dataset', 'load the dataset from file', varargin);
if config.exit
    return;
end

% check folders
in_folder = check_folder('dataset', 'input', config);

% Load sift data from file
fprintf(config.labels);
fprintf('Loading sift data from files...\n');
SiftData = load_sift_data(in_folder, config);