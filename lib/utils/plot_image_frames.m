function image = plot_image_frames(image_id, varargin)
% plot_image_frames
% use plot_image_frames --help or plot_image_frames -h to get help

if(nargin == 0)
  error('missing argument: ''image_id''');
end

config = preset('plot_image_frames', 'plot the frames of a compiled image', varargin);
if config.exit
    return;
end

dataset = load_dataset(config.preset);
[kp_image, kp_cluster] = load_clusters(config.preset);

% check folders
in_folder = check_folder('compiled', 'input', config);

% Load and display image
fprintf(config.labels);
fprintf('Loading image from files...\n');
im = imread(strcat(in_folder, dataset(image_id).name));
imshow(im);

%retrieve keypoints' cluster
% keypoints indices of the image
fprintf(config.labels);
fprintf('Retrieving keypoints indices...\n');
image_kp_idx = find(kp_image == image_id);
% cluster of the keypoints
kp_cluster = kp_cluster(image_kp_idx);

fprintf(config.labels);
fprintf('Displaying the frames...\n');
plotsiftframe(dataset(image_id).frames, 'Labels', kp_cluster);

fprintf(config.labels);
fprintf('Number of frames: %d\n', size(dataset(image_id).frames, 2));