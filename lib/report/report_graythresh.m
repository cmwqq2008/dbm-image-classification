clear;

gray = load_results('imggray');
bn = load_results('imgthresh');

figure
s1 = subplot(2,1,1);
s2 = subplot(2,1,2);

for i = 1:length(gray.classes)
    labels{i} = cell2mat(gray.classes{i}(1));
    values(1,i) = cell2mat(gray.classes{i}(2));
end

for i = 1:length(bn.classes)
    labels2{i} = cell2mat(bn.classes{i}(1));
    values2(i,1) = cell2mat(bn.classes{i}(2));
end

s = bar(s1,values,'EdgeColor',[0.3,0.3,0.3]);
% text(x,values,num2str(values,'%0.2f'),...
% 'HorizontalAlignment','center',...
% 'VerticalAlignment','bottom')
title(s1,'Gray Images');
%legend(s1,'Correctly Matched');
set(s1,'XTickLabel',labels);

x_loc = get(s, 'XData');
y_height = get(s, 'YData');
arrayfun(@(x,y) text(x-0.1, y+0.05,num2str(y*100,'%0.1f%%'), 'Color', 'r', 'Parent', s1), x_loc, y_height);

s = bar(s2,values2,'EdgeColor',[0.3,0.3,0.3]);
x_loc = get(s, 'XData');
y_height = get(s, 'YData');
title(s2,'B/N Images');
set(s2,'XTickLabel',labels2);
arrayfun(@(x,y) text(x-0.1, y+0.05,num2str(y*100,'%0.1f%%'), 'Color', 'r', 'Parent', s2), x_loc, y_height);
%legend(s2,'Correctly Matched');

% labels = [];
% values = [];
% for i = 1:length(bn.classes)
%     labels{i} = bn.classes{i}(1);
%     values(i,1) = cell2mat(bn.classes{i}(2));
%     values(i,2) = 1-cell2mat(bn.classes{i}(2));
% end
%
% rng(0,'twister'); % initialize random number generator
% Y = round(rand(5,3)*10);
%
% bar(s2,values,'stacked');
% title(s2,'B/N Images');
