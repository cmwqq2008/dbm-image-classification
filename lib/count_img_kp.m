function kpn = count_img_kp(SiftData, img_idx)
%% info
% ritorna il numero di differenti keypoints estratti tramite SIFT
% sull'immagine di indice "index", dato il database di dati SIFT
% 
% la struttura di SiftData ? la solita
kpn = size(SiftData(img_idx).frames,2);