% a 1xN cell containing image data
function ImageData = image_cell_resize(ImageDataSrc, numRows, numCols)

% save the rows of image cell in order to know how many images we have
images_n = size(ImageDataSrc,2);

progbar = progressbar_init(50,images_n);

for img_idx = 1:images_n
    % image matrix
    I = cell2mat(ImageDataSrc(img_idx));
    ImageData{img_idx} = imresize(I, [numRows numCols]);
    
    progbar = progressbar_step(progbar);
end