function [ progressbar ] = progressbar_init( bar_length,loop_length )
%PROGRESSBAR_INIT Inizializza una progressbar da usare in un ciclo
%Parametri:
%   bar_length: la lunghezza in caratteri della progressbar
%   loop_length: il numero di cicli che verranno eseguiti dal loop

progressbar = struct;
progressbar.loop = 0;
progressbar.loop_length = loop_length;
progressbar.bar_length = bar_length;
progressbar.update_every = double(loop_length) / bar_length;
progressbar.update_at = progressbar.update_every;
progressbar.step = 0;

 % Stampa il top della progress bar molto rudimentale
fprintf('|');
for i = 1:progressbar.bar_length;
    fprintf('-');
end
fprintf('|\n|');

end

