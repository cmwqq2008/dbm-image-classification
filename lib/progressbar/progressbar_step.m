function progressbar = progressbar_step( progressbar )
%PROGRESSBAR_STEP informa la progressbar che � stato effettuato un ciclo.
%  Se necessario, stampa un "."

progressbar.loop = progressbar.loop + 1;
while progressbar.update_at < progressbar.loop
    progressbar.step = progressbar.step + 1;
    fprintf('.');
    progressbar.update_at = progressbar.update_at + progressbar.update_every;
end
if progressbar.loop >= progressbar.loop_length
    if(progressbar.step < progressbar.bar_length)
        fprintf('.')
    end
    fprintf('|\n');
end