function exist_dir_and_fail( dir_name )
% function exist_dir_and_fail( dir_name )
%   check if dir_name exists and fail with error

if exist(dir_name, 'dir')
    err = MException('OutFolder:exist', 'Output folder (%s) already exists. If you want to overwrite old results, run the script with --force', dir_name);
    throw(err);
end

end

