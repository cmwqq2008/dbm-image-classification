function delete_dir_if_exist_prompt( dir )
%DELETE_DIR_IF_EXIST_PROMPT ask if the user wants to delete a folder

if exist(dir, 'dir')
    in = '';
    while(~strcmp(in,'y') && ~strcmp(in,'n'))
        in = input('Output folder already exists.\nDo you want to overwrite it? [y/n] ', 's');
    end
    if strcmp(in,'y')
        rmdir(dir, 's');
    else
        error('Execution stopped by user.');
    end
end

