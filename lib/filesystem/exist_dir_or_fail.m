function exist_dir_or_fail( dir_name )
% function exist_dir_or_fail( dir_name )
%   check if dir_name exists or fail with error

if ~exist(dir_name, 'dir')
    error('The folder %s does not exist.', dir_name);
end

end

