function [cl_qty] = calc_cluster_qty(kmeans_cluster_n, kmeans_cluster_m, uni_n)

cl_qty = 0;

if (strcmp(kmeans_cluster_m, 'number'))
    cl_qty = kmeans_cluster_n;
elseif (strcmp(kmeans_cluster_m, 'relative_kp'))
    cl_qty = floor(kmeans_cluster_n / 100 * uni_n);
else
    cl_qty = kmeans_cluster_n;
end
